# So you want to run a local copy of the HH website, do you? #

The Hothouse web site and YOU!

### Steps to create your very own Hothouse web site ###
1. Get yourself a LAMP stack - Plenty of resources to do this if you Google, but MAMP for Mac is exceptional. 
2. Follow the LAMP instructions to make sure that: 
    * PHP is installed
    * MySQL is running
    * Apache is running
3. Create a database in MySQL called 'wordpress' (Can you guess what we're going to put in there?)
4. In the /db directory, you'll find a file called 'wordpress.sql'. Using the DB tool of your choice, run that SQL file against your brand new 'wordpress' DB. This will create the structure of the site DB, then populate it.
5. The database is now (hopefully) set up, so now we need to get the file system up and running. The first thing you need to do is to create a wp-config.php file, and adjust the settings to match your database user name / password / host / etc... There is a "wp-config-sample.php" file in the repo that is quite self-explanatory, use this as your starting point.
![wp-config Sample File](/hothouseinc/hothouse-web/raw/d3a44ec4dadd23135ccd2864c6009c4816f70a67/docs/screenshots/wpConfigSample.png?raw=true "wp-config Sample File")


6. Before we go any further, make sure your Apache port is set to "80". (If you're using MAMP, it defaults to 8888, but can be reset in Preferences.)
![Ports in Preferences](/hothouseinc/hothouse-web/raw/535a000f4d1503920d02274571deaee623f52283/docs/screenshots/mampPref.png?raw=true "Ports in Preferences")


7. With that all set, hit your MAMP home page (http://localhost/MAMP/index.php) to make sure everything is still running smoothly.
8. If the MAMP page comes up, your services should all be running, so go to http://localhost in a browser. With any luck, the Hothouse web site (now running locally) should come up and be workable. Look at you! Look what you did!