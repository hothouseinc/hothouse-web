<?php
/*
Plugin Name: Hothouse Ajax Filter
Plugin URI: http://www.hothouseincweb.com
Description: Makes AjAX requests to DB and returns results.
Version: 1.0
Author: Hothouse Inc.
Author URI: http://www.hothouseinc.com
License: Free to Everyone
Copyright 2011  Anthony Baker  (email : abaker@hothouseinc.com)
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
add_action('wp_ajax_' . $_POST['action'], $_POST['functionname']);
add_action('wp_ajax_nopriv_' . $_POST['action'], $_POST['functionname']);
//out puts to print_r with pre tags
function out($content){
		echo "<pre>";
		print_r($content);
		echo "</pre>";
}
 
function filteredCaseStudies() {
	
	$lineBreak = 0;
	
	echo "<div id='workArea'>\n";
	
	if(preg_match("/all\-work/i",$_POST['category_slug'])){
		$filterBy = 'work';
	}
	else{
		$filterBy = $_POST['category_slug'];
	}
	
	$workQuery = get_posts(array('numberposts' =>100,'category_name'=>$filterBy,'orderby'=>'date','order'=>'DESC'));
	
	if(sizeof($workQuery)<1){
		$workQuery = get_posts(array('numberposts' =>100,'category_name'=>'work','orderby'=>'date','order'=>'DESC'));
	}
	
	
	echo "<script type=\"text/javascript\">\n";
	$key = preg_replace("/#/","",$filterBy);
	
	if(preg_match("/all\-work/i",$key)){
		$key ="all-work";
	}
	
	echo "showfilterhighlight('" . $key .  "');\n";
	echo "hashToAddress('" . $key . "');\n";
	echo "</script>\n\n";
	
	
	foreach($workQuery as $workItem){
		//retrieves category for project
		$workItemCats = get_the_category($workItem->ID);
		unset($clientName);
		
		//retrieves clietn name from project categories
		foreach ($workItemCats as $clientCat){
			if($clientCat->parent==22){
				$clientName = (isset($clientCat->name))?$clientCat->name:"";
			}
		}
	
		echo "<div class='workColumn'>\n";
		echo "<span><em>" . $clientName . "</em> <i>|</i> " . $workItem->post_title . "</span>\n\n";
		
		$attachments = attachments_get_attachments($workItem->ID);
		$total_attachments = count($attachments);
		if($total_attachments>0){
		
			for($i=0;$i<$total_attachments;$i++){
			
				if(preg_match("/thumbnail/i",$attachments[$i]["title"])){
					echo "<a href='" . get_permalink($workItem->ID)  . "' onclick=\"javascript: getProjectDetail('" . $workItem->ID .  "'); return false;\"><img src='" . $attachments[$i]["location"] . "' width='257' height='168' alt='" . $attachments[$i]["title"] . "'/></a>\n";
					//echo "<a href='" . get_permalink($workItem->ID)  . "' onclick=\"javascript: getProjectDetail('" . $workItem->ID .  "'); return false;\"><div class='cover'></div><img src='" . $attachments[$i]["location"] . "' width='257' height='168' alt='" . $attachments[$i]["title"] . "'/></a>\n";
				}
			}//end for
		}
		
	echo "</div>\n";
	$lineBreak++;	
	if($lineBreak==3){
		$lineBreak=0;
		echo "<div class='breaker'></div>";
		echo "<div class='workSeparator'></div>";
 	}
	
	}//foreach
	

	echo "<div class='breaker'></div>\n\n";
	echo "</div>\n";	

	echo "<script type=\"text/javascript\">\n";
		echo "_gaq.push(['_trackEvent', 'Work', 'Sorted', '" . htmlspecialchars($filterBy)  ."']);\n";
	echo "</script>\n";
	
	die();
}// end filteredCaseStudies
function caseStudyDetail(){

	if($_POST['useHash']){
		$key = preg_replace("/#/","",$_POST['category_slug']);

		$getPostByName = new WP_Query(array('posts_per_page'=>1,'name' => $key));
		$getPostByName->posts[0]->ID;
		
		$postDetail = get_post($getPostByName->posts[0]->ID);
	}
	else{
		$postDetail = get_post($_POST['post_id']);
	}
	
	
	//retrieves client name
	$postCategories = get_the_category($postDetail->ID);
	foreach($postCategories as $tempCat){
		if($tempCat->category_parent==22){
			$clientname = $tempCat->cat_name;		
		}
	
	}
	
	echo "<div id='workArea'>\n";
	echo "<div id=\"caseimagearea\"></div>\n\n";
	
	//replaces h1 end tag with case scroll area
	if(strlen($postDetail->post_content)>1300){
		if(preg_match("/<\/h1>/i",$postDetail->post_content)){
			$caseContent = $postDetail->post_content;
			$caseContent = preg_replace("/<\/h1>/i","</h1><div class='caseScroll'>",$caseContent);
		}
		else{
			$caseContent = "<div class='caseScroll'>" . $postDetail->post_content . "</div>";
		}
	}
	else{
		$caseContent = $postDetail->post_content;
	}
	
	echo "<div class=\"caseColumn\">" . "<span>" . $clientname  . "</span>" .  $caseContent . "</div>\n\n";
	
	echo "<div class='breaker'></div>\n\n";
	
	echo "</div>\n\n";
	
	echo "<script type=\"text/javascript\">\n";
	
	echo "currentCase = 0\n\n";
	
	echo "hashToAddress('" . $postDetail->post_name . "');\n";

	echo '_gaq.push(["_trackEvent", "Work", "' .  htmlspecialchars($clientname)  . '", "' . htmlspecialchars($postDetail->post_name)  .'"]);' . "\n";
	
	echo "var workImages = Array();\n";
	echo "var fallbackImages = Array();\n";
	
	$attachments = attachments_get_attachments($postDetail->ID);
	$total_attachments = count($attachments);


	if($total_attachments>0){
		
		//pixel width of image
		$imageWidth = 550;
	
		for($i=0;$i<$total_attachments;$i++){
			
			//gets media that does is neither a thumbnail nor a fallback	
			if(!preg_match("/thumbnail|services-section|fallback/i",$attachments[$i]["title"])){
				
				if(preg_match("/video/i",$attachments[$i]["mime"])){
					$videoScript = true;
					$filename = $attachments[$i]["location"];
				}
				elseif(preg_match("/flash/i",$attachments[$i]["caption"])){
					$flashScript = true;
					$filename = $attachments[$i]["location"];
				}
				else{
					$attachments[$i]["location"] = preg_replace("/~hothouse/i","",$attachments[$i]["location"]);
					$filename = "/image.php?width=". $imageWidth . "&image=". $attachments[$i]["location"];
				}
				
				echo "workImages.push(Array(\""  . $filename  . "\",\"". $attachments[$i]["title"]  . "\",\"" .  $attachments[$i]["mime"] . "\",\"" .  $attachments[$i]["caption"] . "\"));\n";
			}
			
			//builds fallback
			if(preg_match("/fallback/i",$attachments[$i]["title"])){
					echo "fallbackImages.push(Array(\""  . $attachments[$i]["location"]  . "\",\"". $attachments[$i]["title"]  . "\",\"" .  $attachments[$i]["mime"] . "\",\"" .  $attachments[$i]["caption"] . "\"));\n";
			}
				
		}//end for
	}
	
	
	//gets category id for navigation filter highlight
	$categoriesForPost = get_the_category($postDetail->ID);
	foreach ($categoriesForPost as $postCat){
		if($postCat->parent==22){
			$filterCat = get_category($postCat->cat_ID);
		}
	}
	echo "showfilterhighlight('" . $filterCat->slug . "');\n\n";
	
	if(sizeof($postDetail)>0){
		echo "createLoadNav();\n";
	}
	else{
		echo "workTriage('all-work');\n";
	}
	echo "</script>\n\n";
	if($videoScript){
		echo "<script language='javascript' type='text/javascript' src='/interface/javascript/video_flow_html5.js'></script>\n";
	}	
	
	
	die();
}
function getFlipBook(){
	
		if($_POST["flipbooktitle"] && preg_match("/flipbook/i",$_POST["flipbooktitle"])){
			$flipbookArticle = new WP_Query(array('posts_per_page'=>1,'post_title'=>$_POST["flipbooktitle"]));
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			
echo<<<XMLOUTPUT1
<PageflipDataSet>
<Settings
	
	CopyrightMessage="BBC1F4F548398C7320B4340428387FE9B6EDFB0E3F9772A9B7CAFB643F867276B6FEFBFDBF55619D30C1F4FCBBA5730EB742FB866722D546275A60EE9102E3162DBA6FDE9ED2D65626426CCD804A7386B6324F8E9DC2E756165A6CA309FC81C5B6A9FB423E3273970D6D0BCB6BFAA949D88728716A563D43E4F925015004FFE9E2DDC7C0BDBD6/1314"
	
	
	PageWidth="$_POST[flipbookwidth]"
	PageHeight="$_POST[flipbookheight]"
	AutoSize="true"
	
	ViewAngle="0"
	
	StartPage="1"
	
	AlwaysOpened="false"
	ZeroPage="false"
	ZeroPageAlwaysVisible="false"
	
	RightToLeft="false"
	VerticalMode="false"
	
	HardCover="false"
	HardPage="false"
	EmbossedPages="true"
	
	LargeCover="false"
	LargeCoverVerticalOversize="32"
	LargeCoverHorizontalOversize="16"
	
	DropShadow="true"
	DropShadowSize="20"
	DropShadowOffset="10"
	DropShadowAlpha="30"
	
	PageLoaderBackColor="0x333333"
	
	PageScale="true"
	MinScale="50"
	MaxScale="200"
	
	MinStageWidth="$_POST[flipbookwidth]"
	MinStageHeight="$_POST[flipbookheight]"
	
	TransparentBackground="true"
	BackgroundColor="0x2A2A2A"
	xBackgroundImageFile=""
	
	FPSControlEnabled="true"
	MaxFPS="60"
		
	ControlBar="true"
	ControlBarScale="70"
	ControlBarFullScreenScale="150"
	
	xControlBarBackgroundColor="0x555555"
	xControlBarBackgroundAlpha="50"
	
	CustomControlBarIcons="false"
	CustomControlBarFile="pageFlipControls.swf"
	
	EnableButtonColoring="true"
	ButtonColor="0x666666"
	ButtonAlpha="100"
	ButtonOverColor="0xF0B400"
	ButtonOverAlpha="100"
	ButtonPressColor="0x666666"
	ButtonPressAlpha="50"
	ButtonDisabledColor="0x666666"
	ButtonDisabledAlpha="15"
	
	ButtonToolTip="true"
	
	ControlBarLoaderEnabled="true"
	ButtonFirstLastEnabled="false"			  	
	ButtonLeftRightEnabled="true"			  	
	ButtonZoomEnabled="false"			  	
	ButtonPrintEnabled="false"			  	
	ButtonAutoFlipEnabled="false"			  	
	ButtonPDFLinkEnabled="false"
	PDFlink=""
	ButtonLanguageEnabled="false"
	ButtonThumbnailEnabled="false"			  	
	ButtonMuteEnabled="false"			  	
	ButtonInfoEnabled="false"			  	
	ButtonIndexEnabled="false"			  	
	ButtonFullScreenEnabled="true"			  	
	ButtonCloseEnabled="false"
	
	ContentPreviewEnabled="true"
	ThumbnailsEnabled="false"
	ThumbnailModeStart="false"
	ThumbnailWidth="90"
	ThumbnailHeight="120"
	ThumbnailDropShadow="true"
	ThumbnailDropShadowSize="8"
	ThumbnailDropShadowOffset="2"
	ThumbnailDropShadowAlpha="30"
	
	StartAutoFlip="false"
	AutoFlipDefaultInterval="2000"
	AutoFlipLooping="true"
		  
	TransparencyEnabled="true"
	TransparencyActivePage="false"
	TransparencyAutoLevel="true"
	TransparencyMaxLevel="16"
	
	DragArea="100"
	AutoFlipArea="100"
	MousePursuitSpeed="8"
	ZoomFollowSpeed="10"
	FlippingDuration="800"
	
	PageCache="5"
	UnloadPages="false"
	
	MouseControl="true"
	DefaultTransition="1"
	DefaultTransitionDuration="250"
	FlippingSpreadActivity="true"
		
	LayoutOrder="PREVIEW-CONTENT-CONTROLBAR"
	ControlBarHeight="55"
	ControlBarOffset="15"
	ContentPreviewHeight="90"
	ContentPreviewWidth="130"
	
	PopupBackgroundColor="0xFFFFFF"
	PopupBackgroundOpacity="80"
	PopupBorderThickness="0"
	PopupBorderColor="0x000000"
	PopupRounded="true"
	PopupRoundedRadius="8"		
	PopupMargin="8"
	PopupTitleFont="Arial"			
	xPopupTextFont=""
	PopupTitleColor="0x333333"
	PopupTextColor="0x333333"
	PopupSpace="8"
	PopupMouseFollowSpeed="5"
	
	InfoWindowWidth="250"
	FloatingWindowColumnWidth="200"
	FloatingWindowMargin="10"
	FloatingWindowHorizontalSpace="16"
	FloatingWindowVerticalSpace="16"
	FloatingWindowSeparator="true"
	FloatingWindowSeparatorColor="0x555555"
	FloatingWindowSeparatorAlpha="100"
	FloatingWindowSeparatorHeight="1"
	FloatingWindowSeparatorOffset="8"
	FloatingWindowBackgroundColor="0x000000"
	FloatingWindowBackgroundAlpha="80"
	FloatingWindowBorderThickness="0"
	FloatingWindowBorderColor="0xFFFFFF"
	FloatingWindowBorderAlpha="100"
	FloatingWindowCornerRadius="16"		
	FloatingWindowColor="0xFFFFFF"
	FloatingWindowSize="10"
	FloatingWindowAlign="CENTER"
	
	TOCTitleFont="Arial"			
	TOCTitleColor="0xFFFFFF"
	TOCTitleSize="20"
	TOCTitleAlign="CENTER"			
	TOCLinkColor="0xFFFFFF"
	TOCLinkSize="12"
	TOCLinkAlign="CENTER"			
	TOCLinkPageNumberColor="0xFFFFFF"
	TOCLinkPageNumberSize="20"
	TOCLinkPageNumberAlign="CENTER"
	TOCPageNumberFirst="false"
	TOCPageNumberWidth="32"		
	TOCDescriptionColor="0xFFFFFF"
	TOCDescriptionSize="12"
	TOCDescriptionAlign="CENTER"
	
	SoundEnabled="false"
	StartMute="true" >
	<Sounds	EffectVolume="50" >
	</Sounds>
</Settings>
<PageOrder>
XMLOUTPUT1;
			$attachments = attachments_get_attachments($flipbookArticle->ID);
			$total_attachments = count($attachments);
			
			if($total_attachments>0){
			for($i=0;$i<$total_attachments;$i++){
				
				echo "<PageData  PageFile='" . $attachments[$i]["location"] . "' />\n" ;
	
			}//end for
			
			}// end if
echo<<<XMLOUTPUT2
</PageOrder>
<Language>
]	<Lang Name="English"
		Loading="LOADING"
		PagerPage="Page #"
		PagerThumbnailPopup="Page #"
		PagerThumbnails="Thumbnails"
		PagerThumbnailPages="Thumbs #"
		PagerZoomPage="Zoom Page #"
		
		PrintLeftTooltip="Print left side page"
		PrintRightTooltip="Print right side page"
		ZoomLeftTooltip="Zoom on left side page"
		ZoomRightTooltip="Zoom on right side page"
		ZoomInTooltip="Zoom In"
		ZoomOutTooltip="Zoom Out"
		FirstPageTooltip="First page"
		PreviousPageTooltip="Previous page"
		NextPageTooltip="Next page"
		LastPageTooltip="Last page"
		
		AutoFlipTooltip="Autoflip On/Off (A)"
		MuteTooltip="Mute (M)"
		InfoTooltip="About (Shift-I)"
		IndexTooltip="Table Of Contents (I)"
		LanguageTooltip="Magyar"
		ThumbnailTooltip="Thumbnail View (T)"
		FullscreenTooltip="Fullscreen On/Off (Shift-F)"
		CloseTooltip="Quit PageFlip (Shift-Q)"
		PDFDownloadTooltip="Download PDF version"
		
		SearchTooltip="Find"
		HelpTooltip="Help"
		BookmarksTooltip="Bookmarks"
	/>
</Language>
</PageflipDataSet>
XMLOUTPUT2;
			
		}// end if
					
	die();
}// end getflipbook
function getNewsContent(){
		
	if($_POST["month"] || $_POST["year"]){
		$newsPosts = new WP_Query(array('category_name'=>'news','posts_per_page'=>-1,'monthnum'=>$_POST["month"],'year'=>$_POST["year"]));
	}
	else if($_POST["fb"]){
		$fb = preg_replace("/#fb/","",$_POST["fb"]);
		$newsPosts = new WP_Query(array('category_name'=>'news','posts_per_page'=>1,'p'=>$fb ));
	}
	else{
		$today = getdate();
		//$newsPosts = new WP_Query(array('category_name'=>'news','posts_per_page'=>3,'orderby' => 'date', 'order' => 'DESC'));
		$newsPosts = new WP_Query(array('category_name'=>'news','year'=>$today["year"],'posts_per_page'=>-1,'orderby' => 'date', 'order' => 'DESC'));
	}
	
	if(sizeof($newsPosts->posts)==0){
		$newsPosts = new WP_Query(array('category_name'=>'news','posts_per_page'=>5,'orderby' => 'date', 'order' => 'DESC'));
	}
	
	if(sizeof($newsPosts->posts)>0){
		$linecount=1;
		
		foreach($newsPosts->posts as $apost){
			
			echo '<div class="contentArticleSection">' . "\n";
			//echo "<div id='postTitleDate'>"  . date('F j, Y', strtotime($apost->post_date)) . "</div>\n\n";
			echo "<div id='postTitleDate'>"  . date('F Y', strtotime($apost->post_date)) . "</div>\n\n";
			
			if($apost->post_title && !preg_match("/\<h1/i",$apost->post_content) && !preg_match("/hide/i",$apost->post_title)){
					echo "<div id='postTitle'>" . $apost->post_title . "</div>\n\n";
			
			}//end if
				
			
				
			//retrieves img src from content using a non-greedy quanitifier
			preg_match("/src=['\"](.*?)['\"]/i",$apost->post_content,$matches);
			//replaces the image source with the holder graphic and adds the original source as a data-original attribute.
			
			//creates the new source tag for the lazy loader
			$newsource = "<img src='/interface/images/lazy_box.gif'" . " data-original='" . $matches[1] . "' class='lazy' ";
					
			//removes the image source
			$apost->post_content = str_replace($matches[0],"",$apost->post_content);
			
			//puts a new image source in place for the lazy loader code.
			$apost->post_content = str_replace("<img",$newsource,$apost->post_content);


			if($vine = get_post_meta($apost->ID,'twitter_vine_video',true)){

				$vine = preg_replace("/(width|height)=\"[0-9]{3}\"/mi","$1=\"500\"",$vine);
				$vine = preg_replace("/audio=1/mi","audio=0",$vine);
				$apost->post_content = $vine . $apost->post_content;
			}
			
			//for some reason the src="" was not being replaced correctly in a one line preg_replace.
			// the class lazy was not being inserted on some images inserted via an IE browser on the PC.
				
	
			//$apost->post_content = preg_replace("/src=['\"].*?['\"]/i",$newsource,$apost->post_content);
			
			//replaces h1 end tag with case scroll area
			if(get_post_meta($apost->ID,'scrollable',true)){
				$caseContent = $apost->post_content;
				$caseContent = preg_replace("/<\/h1>/i","</h1><div class='newsCaseScrollable'>",$caseContent);
				echo $caseContent . "<div class='breaker'></div></div>";
			}
			else{
				$apost->post_content = preg_replace("/<\/h1>/i","</h1><div class='sectionContentColumn'>",$apost->post_content);
				echo $apost->post_content . "<div class='breaker'></div></div>";
			}


				
			echo "<div class='breaker'></div>\n";

			echo "</div>\n"; //end contentArticleSection

			
			if($linecount < sizeof($newsPosts->posts)){
				echo "<div class='workSeparator' " .  (($linecount>1)?"style='margin-bottom:0px;'":"") . "></div>\n\n";
			}
			
			if($linecount>1){
				echo '<a href="#top" class="topButton">Top of Page</a>' . "\n\n";
			}
			
			$linecount++;
		}//end foreach
		
		//add javascript
		echo '<script src="/interface/javascript/jquery.lazyload.min.js" type="text/javascript"></script>' ."\n";
		echo '<script type="text/javascript">' ."\n\n";
		echo 'j("img.lazy").lazyload({effect : "fadeIn"});' ."\n\n";
		echo 'if(browsersniffer){$(".topButton").bind("click",function(){$(document).scrollTop(0);return false;});}' ."\n\n";

		if(isset($fb)){
			echo "_gaq.push(['_trackEvent', 'News', 'Front Burner', '" . htmlspecialchars($apost->post_title)  ."']);\n";
		}

		if($_POST["month"] || $_POST["year"]){
			echo "_gaq.push(['_trackEvent', 'News', 'Front Burner', '" . htmlspecialchars($apost->post_title)  ."']);\n";
		}

		if($_POST["month"] || $_POST["year"]){
			echo "_gaq.push(['_trackEvent', 'News', 'By Month and Year', 'Month: " . ((isset($_POST["month"]))?$_POST["month"]:"None Selected")  ." Year:" . ((isset($_POST["year"]))?$_POST["year"]:"None Selected") .  "']);\n";
		}


		echo '</script>' ."\n\n";
		
	}
	else{
		echo "<div id='postTitle' style='height:400px;'>No Articles Found</div>\n\n";
	}
	die();
}
function getStaffBio(){
	
	
		if($_POST["staffbio"] && !preg_match("/all/i",$_POST["staffbio"])){
			$staffNav = new WP_Query(array('posts_per_page'=>1,'name'=>$_POST["staffbio"]));
			
			echo "<div id='staffContent'>\n";
			
			if($staffNav->posts[0]->post_title && !preg_match("/\<h1/i",$staffNav->posts[0]->post_content) && !preg_match("/hide/i",$staffNav->posts[0]->post_title)){
				echo "<div id='postTitle'>" . $staffNav->posts[0]->post_title . "</div>\n";
			}

			$staffNav->posts[0]->post_content = preg_replace("/<\/h1>/i","</h1><div class='sectionContentColumn'>",$staffNav->posts[0]->post_content);
			echo $staffNav->posts[0]->post_content . "<div class='breaker'></div></div>";
			
			echo "</div>\n";

			echo "<div class='breaker'></div>\n";
			
			echo "<script type=\"text/javascript\">\n";
			echo "hashToAddress('" . $staffNav->posts[0]->post_name . "');\n";

			echo "_gaq.push(['_trackEvent', 'About', 'Staff Bio', '" . htmlspecialchars($staffNav->posts[0]->post_name)  ."']);\n";

			echo "</script>\n";
			
		}
	
		else{
			$staffNav = get_posts(array('numberposts'=>-1,'category_name'=>'people','orderby'=>'id','order'=>'ASC'));
				
			$lineBreak=0;
			$recordCounter = 0;
				
				
			echo "<div id='workArea'>\n";
            
			foreach($staffNav as $staffItem){
				
			echo "<div class='workColumn'>\n";
					
					$attachments = attachments_get_attachments($staffItem->ID);
					$total_attachments = count($attachments);
					if($total_attachments>0){
					
						for($i=0;$i<$total_attachments;$i++){
						
							if(preg_match("/thumbnail/i",$attachments[$i]["title"])){
							//	echo "<a href='' onclick=\"javascript: getStaffBio('". $staffItem->post_name ."'); return false;\"><div class='cover'></div><img src='" . $attachments[$i]["location"] . "' width='257' height='168' alt='" . $attachments[$i]["title"] . "'/></a>\n";
								echo "<a href='' onclick=\"javascript: getStaffBio('". $staffItem->post_name ."'); return false;\"><img src='" . $attachments[$i]["location"] . "' width='257' height='168' alt='" . $attachments[$i]["title"] . "'/></a>\n";
							}
						}//end for
					}// end if
					
					echo "<div class='employeeTitleSmall'><span>" . $staffItem->post_title . "</span>\n\n";
					
					$personTitle = get_post_meta($staffItem->ID,'employeeTitle',true);
					if($personTitle){
						echo $personTitle;
					}
					
					echo "</div>\n";
					
				
					echo "</div>\n";
				
					$lineBreak++;	
					$recordCounter++;	
				
					if($lineBreak==3 && $recordCounter<sizeof($staffNav)){
						$lineBreak=0;
						echo "<div class='breaker'></div>";
						echo "<div class='workSeparator'></div>";
					}
	
			}//foreach
			
	
			
			echo "</div><!-- End People Area -->\n";
			
echo<<<ENDSCRIPT
<script type="text/javascript">;
if(!browsersniffer){
	j(".workColumn").bind('mouseover', function(){
		j(this).find(".cover").fadeTo(200,.8);
	});
	
	j(".workColumn").bind('mouseout', function(){
		j(this).find(".cover").hide();
	});
}
hashToAddress('all-staff');
</script>
ENDSCRIPT;
			
		}
	die();
}// end filteredCaseStudies
function getExpertise(){
	
	if($_POST["expertSection"]){
			$expertContent = new WP_Query(array('posts_per_page'=>1,'name'=>$_POST["expertSection"]));
			
			
			if($expertContent->posts[0]->post_title && !preg_match("/\<h1/i",$expertContent->posts[0]->post_content) && !preg_match("/hide/i",$expertContent->posts[0]->post_title)){
				echo "<div id='postTitle'>" . $expertContent->posts[0]->post_title . "</div>\n\n";
	
			}//end if
			
			echo $expertContent->posts[0]->post_content;

			echo "<script type=\"text/javascript\">\n";
			echo "_gaq.push(['_trackEvent', 'Services', '" . htmlspecialchars($_POST["expertSection"]) . "', '" . htmlspecialchars($expertContent->posts[0]->post_title)  ."']);\n";
			echo "</script>\n";

	}
	die();
}
?>