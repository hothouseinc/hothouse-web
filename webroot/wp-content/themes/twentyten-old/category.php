<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
get_header(); ?>
<?php
function drawSlideShow($slides){
	
	$total_slides = count($slides);
	
	echo "\n\n<script type='text/javascript'>\n";
	echo "var slides = Array();\n";
	
	if($total_slides>0){
		for($i=0;$i<$total_slides;$i++){
			if(preg_match("/slideshow/i",$slides[$i]["caption"])){
			 echo "slides[" . $i . "] = Array('" .  $slides[$i]["location"] . "','" . $slides[$i]["title"] .  "');\n";
			}
		}//end for
	}
	
	echo "createSlideShow(slides);\n";
	echo "</script>\n\n";
	
}
		$cat = get_query_var('cat');
		$catID =  get_category($cat);
		
		if($catID->parent==0){
			$introPost = get_posts(array('numberposts' => 1,'category'=>$catID->cat_ID,'meta_key' =>'defaultCategoryPage','meta_value' =>'true'));
		}
		else{
			$introPost = get_posts(array('numberposts' => 1,'category'=>$catID->cat_ID,'meta_key' =>'sectionIntro','meta_value' =>'true'));
		}
		if(sizeof($introPost)==0){
			$introPost = get_posts(array('numberposts' => 1,'category'=>$catID->cat_ID,'orderby'=>'id','order'=>'ASC'));
		}
		//shows content for sections that are not: work, people nor clients
		if(!preg_match("/work|people|clients|expertise|services/",$catID->slug)){
		
			$introCategory = get_the_category($introPost[0]->ID);
			
			//$subnavigation = new WP_Query('cat=' . $introCategory[0]->cat_ID);
	
			$subnavigation = get_posts(array('numberposts'=>500,'category'=>$introCategory[0]->cat_ID,'orderby'=>'id','order'=>'DESC'));
	
			if($introCategory[0]->category_description){
				echo "<div id='pageTitle'>";

				if(preg_match("/news/i",$introCategory[0]->cat_name)){
					echo $introCategory[0]->cat_name;
				}
				else{
					echo $introCategory[0]->category_description;
				}

				echo "</div>\n";
			}
	
	
			if(sizeof($subnavigation)>1){	
			
			echo "<script type='text/javascript'>j(document).ready(function(){\n";
			
			if($catID->slug == 'news'){
				echo "newsDropDown();\n";
				echo "newsTriage();\n";
			}
			else{
				echo "activateDropDown();\n";
			}
	
			
			echo "});</script>\n\n";
			
			if(preg_match("/news/",$catID->slug)){
				
				$indice = 0;
				$datesArray = array();
				
				
				//construct dates array
				foreach($subnavigation as $subpost){
					$tempMonth = mysql2date("n", $subpost->post_date);
					$tempMonthName = mysql2date("F", $subpost->post_date);
					$tempYear = mysql2date("Y", $subpost->post_date);
	
					if($tempMonth!=$datesArray[$indice-1]["month"]){
						$datesArray[$indice] = array("month"=>$tempMonth,"monthName"=>$tempMonthName,"year"=>$tempYear);
						$indice++;	
					}
					else if($tempMonth==$datesArray[$indice-1]["month"] && $tempYear!=$datesArray[$indice-1]["year"]){
						$datesArray[$indice] = array("month"=>$tempMonth,"monthName"=>$tempMonthName,"year"=>$tempYear);
						$indice++;	
					}
				}
				
				
				echo "<div id='sectionSubNav' class='typefilter'>\n";
				echo "<span class='filterTitle'>Select a Date:</span>\n";
				echo "<a class='filterSelected' id='filselec-month'>Month</a>\n";
				
				echo "<div id='filtdrop-month' class='filterdrop'>\n";
				echo "<span class='filterSelected'>Month</span>\n";
				echo "<a href='' id='f-all-month' onclick='javascript: return false;'>All Posts</a>\n";
				
				for($i=0;$i<sizeof($datesArray);$i++){
					if($datesArray[$i]["month"]!=$datesArray[$i-1]["month"]){
						echo "<a href='' id='f-".  $datesArray[$i]["month"] . "' onclick='javascript: return false;'>" . $datesArray[$i]["monthName"] . "</a>\n";
					}
				}
				echo "</div>\n\n";
				
		
				echo "<a class='filterSelected' id='filselec-year'>Year</a>\n";
				
				echo "<div id='filtdrop-year' class='filterdrop'>\n";
				echo "<span class='filterSelected'>Year</span>\n";
				echo "<a href='' id='f-all-year' onclick='javascript: return false;'>All Posts</a>\n";
				for($i=0;$i<sizeof($datesArray);$i++){
					if($datesArray[$i]["year"]!=$datesArray[$i-1]["year"]){
						echo "<a href='' id='f-".  $datesArray[$i]["year"] . "' onclick='javascript: return false;'>" . $datesArray[$i]["year"] . "</a>\n";
					}
					
				}
				echo "</div>\n\n";
				
				echo "<a href=''  class='filterSelected' id='newssubmit' onclick=\"javascript: newsTriage(j('#filselec-month').attr('title'),j('#filselec-year').text()); return false;\">Submit</a>\n";
	
				echo "</div><!-- end sectionSubNav  -->\n\n";		
				
			
			}
			else{
				
				echo "<div id='sectionSubNav' class='typefilter'>\n";
				echo "<span class='filterTitle'>Select a Date:</span>\n";
				echo "<a class='filterSelected' id='filselec-month'>" . $introCategory[0]->category_description . "</a>\n";
				
				echo "<div id='filtdrop' class='filterdrop'>\n";				
				foreach($subnavigation as $subpost){
					echo "<a href='' id='f-".  $subpost->ID . "' onclick=\"javascript: workTriage('". $subpost->ID ."'); return false;\">" . $subpost->post_title . "</a>\n";
				}
				echo "</div>\n\n";
				
				echo "</div><!-- end sectionSubNav  -->\n\n";	
			}
				
			echo "<div class='breaker'></div>\n\n";				
	
			}
	
		
			
			/*
			if(sizeof($subnavigation)>1){
			echo "<div id='sectionSubNav'>\n";
	
				foreach($subnavigation as $subpost){
					echo "<a href='/" . $subpost->post_name ."' id='pm_".  $subpost->ID . "'>" . $subpost->post_title . "</a>\n";
				}
					
			echo "</div><!-- end sectionSubNav  -->\n\n";
			echo "<div class='breaker'></div>\n\n";
			}
			*/	
	
			echo "<div id='content'>\n\n";	
	
	
			//echo "<div id='sectionContent'" . ((sizeof($subnavigation)==1)?"style='width:1200px;'":"") . ">\n";
			echo "<div id='sectionContent'>\n";
	
			if($catID->slug != 'news'){
					if($introPost[0]->post_title && !preg_match("/\<h1/i",$introPost[0]->post_content) && !preg_match("/hide/i",$introPost[0]->post_title)){
						echo "<div id='postTitle'>" . $introPost[0]->post_title . "</div>\n\n";
					}
					echo $introPost[0]->post_content;
					
					if(preg_match("/slideshow/i",$introPost[0]->post_content)){
						drawSlideShow(attachments_get_attachments($introPost[0]->ID));
					}
					
					
			}//end if
	
			
	
			
			echo "</div><!-- end sectionContent  -->\n";
	
		}// end if
		
		
		//display people content menu
		else if(preg_match("/people/",$catID->slug)){
print<<< endScript
<script type="text/javascript">
j(document).ready(
function(){
		
		makeDropDown();
		getStaffBio();
});
</script>
			
endScript;
			
			if($catID->description){
			echo "<div id='pageTitle'>" . $catID->description ."</div>\n";
			}
			
            $staffNav = get_posts(array('numberposts'=>100,'category_name'=>$catID->slug,'orderby'=>'id','order'=>'ASC'));
            
            
            echo "<div id='sectionSubNav' class='typefilter'>\n";
			echo "<span class='filterTitle'>Select:</span>\n";
			echo "<a class='filterSelected' id='filselec'>The Team</a>\n";
                echo "<div id='filtdrop' class='filterdrop'>\n";				
                echo "<a href='' onclick=\"javascript: getStaffBio('all-staff'); return false;\">The Team</a>\n";
               foreach($staffNav as $staffItem){
                    echo "<a href='' onclick=\"javascript: getStaffBio('". $staffItem->post_name ."'); return false;\">" . $staffItem->post_title . "</a>\n";
                }
                echo "</div>\n\n";
                
	
			echo "</div><!-- end sectionSubNav  -->\n\n";	
            
			echo "<div class='breaker'></div>\n\n";
			
			echo "<div id='content'>\n\n";	
			echo "<div id='workContent'>\n";
	
			echo "</div><!-- end workContent  -->\n";
        	
        
		}// end else if
		
		
		//display clients menu
		else if(preg_match("/clients/",$catID->slug)){
			
print<<< endScript
<script type="text/javascript">
j(document).ready(
function(){	
       	j('body').append("<a href='' id='clientPointer'>See Work</a>");
 
   		if(!browsersniffer){
            j(".logocover").bind('mouseover', function(){
                j(this).css("background-position", "0px -168px");
            });
                
            j(".logocover").bind('mouseout', function(){
                j(this).css("background-position", "0px 0px");
                j("#clientPointer").css({"display":"none"});
            });
            
        
			j(".logocover").bind('mousemove',function(e){ 
            	var hasLink = j(this).attr("href");
            	if(Boolean(hasLink)){
                	j("#clientPointer").attr("href",String(hasLink));
                	j("#clientPointer").css({"display":"block","left":Number(e.pageX) - (j("#clientPointer").width()/2) + "px","top":Number(e.pageY) + 20 + "px"});
                }
                
			});
        }
});
</script>
			
endScript;
			if($catID->description){
			echo "<div id='pageTitle'>" . $catID->description ."</div>\n";
			}
			
			echo "<div class='breaker'></div>\n\n";
			
			$clientsNav = get_posts(array('numberposts'=>100,'category'=>$catID->cat_ID,'orderby'=>'title','order'=>'ASC'));
			
	
			echo "<div id='content'>\n\n";	
			echo "<div id='workContent'>\n";
			echo "<div id='workArea'>\n";
			
			$lineBreak=0;
			$recordCounter = 0;			
            
			foreach($clientsNav as $clientItem){
			
				echo "\n<div class='workColumn'>\n";
								
				$attachments = attachments_get_attachments($clientItem->ID);
				$total_attachments = count($attachments);
				if($total_attachments>0){
                      
                    $projectLink = get_post_meta($clientItem->ID, 'projectpage', true); 
                     
                     echo "<a ". (($projectLink)?"href='/category/work/" . $projectLink . "'":"") . " class='logocover' style=\"background-image:url(" . $attachments[0]['location'] . ");\"></a>\n";
				
					
				}// end if
			
				echo "</div>\n";
			
				$lineBreak++;
                $recordCounter++;	
			
				if($lineBreak==3 && $recordCounter<sizeof($clientsNav)){
					$lineBreak=0;
					echo "<div class='breaker'></div>";
                    echo "<div class='workSeparator'></div>";
				}
			
			
			}//foreach
		
		
			echo "</div><!-- End Client Area -->\n";
            
            echo "</div><!-- end workContent  -->\n";
		
		}// end else if
		
        
        //display clients menu
		else if(preg_match("/expertise|services/",$catID->slug)){
        
        	echo "<script type='text/javascript' src='/interface/javascript/expertise.js'></script>";
		
			$introCategory = get_the_category($introPost[0]->ID);
			
			$subnavigation = get_posts(array('numberposts'=>0,'category'=>$introCategory[0]->cat_ID,'orderby'=>'id','order'=>'ASC'));
	
			if($introCategory[0]->category_description){
			echo "<div id='pageTitle'>" . $introCategory[0]->category_description ."</div>\n";
			}
	
			echo "<div id='content'>\n\n";	
            
			echo "<div id='expertContent'>\n";
            
       		echo "<div id='expertNav'>\n";
	
			foreach($subnavigation as $subpost){
            	if(!preg_match("/introduction/i",$subpost->post_name)){
					echo "<a href='/" . $subpost->post_name ."' onclick=\"javascript: getExpertise('" . $subpost->post_name ."'); return false;\"><span id='".  $subpost->post_name . "'>" . $subpost->post_title . "</span></a>\n";
				}
            }
					
			echo "</div><!-- end expertNav  -->\n\n";
    
            
            echo "<div class='expertColumn'></div>\n";
            
			echo "</div><!-- end expertContent  -->\n";
	
		}// end expertise
		
		//display work menu
		else{
	
            echo "<script type='text/javascript' src='/interface/javascript/workfilter.js'></script>\n\n";		
			           
			
			echo "<div id='pageTitle'>" . $catID->category_description ."</div>\n";
			$getworkcategories = get_categories(array('child_of'=>$catID->cat_ID,'exclude'=>'22','orderby'=>'slug'));
			$getclientcategories = get_categories(array('parent'=>22,'orderby'=>'slug'));
			
			
			echo "<div id='sectionSubNav' class='typefilter'>\n";
			echo "<span class='filterTitle'>Sort:</span>\n";
			echo "<a href='' class='filterSelected' id='f-all-work' onclick=\"javascript: showAllWork(); return false;\" style='color:#acabac;'>All Work</a>\n";
			
			if(sizeof($getworkcategories)>1){
				echo "<a class='filterSelected' id='filselec-type'>Industry</a>\n";
				echo "<div id='filtdrop-type' class='filterdrop'>\n";
                echo "<span class='filterSelected'>Industry</span>\n";
				foreach($getworkcategories as $subpost){
					if($subpost->slug!="all-work"){
					echo "<a href='' id='f-".  $subpost->slug . "' onclick=\"javascript: workTriage('". $subpost->slug ."'); return false;\">" . $subpost->name . "</a>\n";
					}
				}
				
				echo "</div>\n\n";
			}
			
			if(sizeof($getclientcategories)>1){
				echo "<a class='filterSelected' id='filselec-client'>Client</a>\n";
				echo "<div id='filtdrop-client' class='filterdrop'>\n\n";
				
                echo "<span class='filterSelected'>Client</span>\n";
				foreach($getclientcategories as $subpost){
					if(!preg_match("/hothouse/i",$subpost->slug)){
						echo "<a href='' id='f-".  $subpost->slug . "' onclick=\"javascript: workTriage('". $subpost->slug ."'); return false;\">" . $subpost->name . "</a>\n";
					}
				}
				
				echo "</div>\n\n";
			}
			
	
	
   			echo "<div class='breaker'></div>\n\n";				
			echo "</div><!-- end sectionSubNav  -->\n\n";		
			
			echo "<div id='content'>\n\n";	
			echo "<div id='workContent'></div>\n";
		}// end else
		
		
		if($introCategory[0] && $introCategory[0]->slug != $_SESSION["category"]){
			$_SESSION["subcategory"] = $introCategory[0]->slug;
		}
		else{
			unset($_SESSION["subcategory"]);
		}
		$_SESSION["post"] = $introPost[0]->ID;
?>
<?php	
if($catID->parent>0){
	$temp = get_category($catID->parent);
	$_SESSION["category"] = $temp->slug;
	if(preg_match("/people/",$catID->slug)){
		$_SESSION["subcategory"] = $catID->slug;
	}
}
else{
	$_SESSION["category"] = $catID->slug;
	unset($_SESSION["subcategory"]);
	$_SESSION["subcategory"] = $introCategory[0]->slug;
}
echo "<div class='breaker'></div>\n";
echo "</div><!-- end content  -->\n";
get_footer(); 
?>
