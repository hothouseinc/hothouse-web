<?php

/**

 * The template used for displaying page content in page.php

 *

 * @package WordPress

 * @subpackage Twenty_Twelve

 * @since Twenty Twelve 1.0

 */

?>



	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="entry-header">
		<script type="text/javascript" src="/interface/javascript/workfilter.js"></script>
			<?php if ( ! is_page_template( 'page-templates/front-page.php' ) ) : ?>

			<?php the_post_thumbnail(); ?>

			<?php endif; ?>

			<h1 id="servicesTitle"><?php the_title(); ?></h1>

			<div id="sectionSubNav" class="typefilter nobg subnavmods">
			<span class="filterTitle">Services:</span>
			<a class="filterSelected" id="filselec-client">Entertainment Marketing</a>
			<div id="filtdrop-client" class="filterdrop arrowup filterArrowUp" style="display: none; left: 198px; top: 65px;">
 			<?php wp_nav_menu( array('menu' => 'servicesdetailsnav' )); ?>
			</div>
			</div>

		</header>



		<div class="entry-content">
		
		<div id="serviceContent">

		<div id="mainContent">

                <div id="detailsmainimage"><img src="<?php the_field('main_image'); ?>"/></div>

		<div id="detailstext"><div class="orangedetails"><?php the_field('highlight_text'); ?></div>

                <p><?php the_content(); ?></p>

                </div> <!-- End #detailstext -->

		</div> <!-- End MainContent -->

		<br style="clear:both">


<div id="featuredwork">

<p style="position: relative; top:10px; font-family:MuseoSans500;">Featured Work</p>

<! ---- Feature 1 ------>
		<div class="featureblock bar" style="margin-right:42px;">
        	<a href="<?php the_field('feature_link_1'); ?>" class="featureoverlay">
		<div class="details">
		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         	<span class="orange"><?php the_field('network_1'); ?></span> <span style="color:#bce0cd !important;"> | </span> <span style="color:#ffffff !important;"><?php the_field('show_or_movie_title_1'); ?></span></p>
		</div> <!-- End #details -->
                </a>
		<div class="featurebackdrop"><img src="<?php the_field('feature_image_1'); ?>" style="padding-top:0px !important;"/></div><!-- End #featurebackdrop -->
		</div> <!-- End #featureblock -->

<! ---- Feature 2 ------>
<div class="featureblock bar">
        	<a href="<?php the_field('feature_link_2'); ?>" class="featureoverlay">
		<div class="details">
		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         	<span class="orange"><?php the_field('network_2'); ?></span> <span style="color:#bce0cd !important;">|</span> <span style="color:#ffffff !important;"><?php the_field('show_or_movie_title_2'); ?></span></p>
		</div>
                </a>
		<div class="featurebackdrop"><img src="<?php the_field('feature_image_2'); ?>" style="padding-top:0px !important;"/></div>
		</div>

<! ---- Feature 3 ------>
<div class="featureblock bar" style="float:right;">
        	<a href="<?php the_field('feature_link_3'); ?>" class="featureoverlay">
		<div class="details">
		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         	<span class="orange"><?php the_field('network_3'); ?></span> <span style="color:#bce0cd !important;">|</span> <span style="color:#ffffff !important;"><?php the_field('show_or_movie_title_3'); ?></span></p>
		</div>
                </a>
		<div class="featurebackdrop"><img src="<?php the_field('feature_image_3'); ?>" style="padding-top:0px !important;"/></div>
		</div>

</div> <!--Featured Items -->
<br style="clear:left;"/>
</div> <!-- End serviceContent -->

         

		</div><!-- .entry-content -->

		<footer class="entry-meta">

			

		</footer><!-- .entry-meta -->

	</article><!-- #post -->