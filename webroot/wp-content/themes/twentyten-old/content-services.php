<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php if ( ! is_page_template( 'page-templates/front-page.php' ) ) : ?>
			<?php the_post_thumbnail(); ?>
			<?php endif; ?>
			<h1 id="servicesTitle"><?php the_title(); ?></h1>
		</header>

		<div class="entry-content">
		<div id="workContent" style="padding-top:20px !important;">
		<?php // the_content(); ?>
        <?php $services_loop = new WP_Query( array('post_type' => 'services', 'posts_per_page' => '20', 'orderby' => 'menu_order' ) ); ?>
 
        <?php while ( $services_loop->have_posts() ) : $services_loop->the_post(); ?>
        <!--Bar-->
		<div class="mosaic-block bar">
        <a href="<?php the_field('service_page_link'); ?>" class="mosaic-overlay">
				<div class="details">
		 		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         		<span class="orange"><?php the_field('service_title'); ?> </span><br /><br />
		 		<span style="color:#fff !important;"><?php the_field('service_excerpt'); ?></span><br /><br />
         		<span class="learnmore">Learn More <img src="/wp-content/themes/twentyten/images/arrow.png" style="top: 2px; position: relative;" /></span> </p>
				</div>
                </a>
			<div class="mosaic-backdrop"><img src="<?php the_field('service_thumbnail'); ?>"/></div>
		</div>

		 <?php endwhile; ?>
         
         <br style="clear:left;"/>
         
</div> <!-- End workContent -->
		</div><!-- .entry-content -->
		<footer class="entry-meta">
			
		</footer><!-- .entry-meta -->
	</article><!-- #post -->