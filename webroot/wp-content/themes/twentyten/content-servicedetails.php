<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
 
if(get_field('vimeo_id')){
	$main_content = '<iframe class="vimeo" src="//player.vimeo.com/video/' . get_field('vimeo_id') . '" width="585" height="464" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
	$show_featured_work = "style='display:none;'";
}
else{
	$main_content = "<img src='" . get_field('main_image') . "'/>";
	$show_featured_work = "";
}
 
 
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
		<script type="text/javascript" src="/interface/javascript/workfilter.js"></script>
			<?php if ( ! is_page_template( 'page-templates/front-page.php' ) ) : ?>
			<?php the_post_thumbnail(); ?>
			<?php endif; ?>
			<h1 id="servicesTitle"><?php the_title(); ?></h1>
			<div id="sectionSubNav" class="typefilter nobg subnavmods">
			<span class="filterTitle">Services:</span>
			<a class="filterSelected" id="filselec-client">Make a Selection</a>
			<div id="filtdrop-client" class="filterdrop arrowup filterArrowUp" style="display: none; left: 198px; top: 65px;">
 			<?php wp_nav_menu( array('menu' => 'servicesdetailsnav' )); ?>
			</div>
			</div>
		</header>
		<div class="entry-content">
		
		<div id="serviceContent">
		<div id="mainContent">
                <div id="detailsmainimage"><?php echo $main_content; ?></div>
		<div id="detailstext"><div class="orangedetails"><?php the_field('highlight_text'); ?></div>
                <p><?php the_content(); ?></p>
                </div> <!-- End #detailstext -->
		</div> <!-- End MainContent -->
		<br style="clear:both">
<div id="featuredwork" <?php echo $show_featured_work; ?>>

<p style="position: relative; top:10px; font-family:MuseoSans500;">Featured Work</p>


<?php
$postid = get_the_ID();
$mypost = get_post($postid);
$servicequery = get_posts(array('numberposts'=>3,'orderby'=>'date','order'=>'DESC','meta_key' =>'feature-in-services','meta_value' =>$mypost->post_name));
$columncount = 1;

foreach($servicequery as $item){

	$categories = get_the_category($item->ID);

	//go through categories and finds if this is a news item to generate hyperlink
	foreach($categories as $cat){
		if(preg_match("/news/i",$cat->slug)){
			$isnews = true;
		}
		else{
			$isnews = false;	
		}

		if($cat->parent==22){
			$clientName = (isset($cat->name))?$cat->name:"";
		}
	}

	//creates links for work item or for news item
	if($isnews){
		$feature_link = "/category/news/#fb" . $item->ID;
	}
	else{
		$feature_link = "/category/work/#" . $item->post_name;
	}

	$attachments = attachments_get_attachments($item->ID);
	$total_attachments = count($attachments);
	if($total_attachments>0){
		unset($feature_image);
		//gets service thumbnail if available
		foreach($attachments as $img){

			if(preg_match("/service/i",$img["title"])){
				$feature_image = $img["location"];
			}
		}

		//gets regular thumbnail if not available
		if(!$feature_image){
			foreach($attachments as $img){
				if(preg_match("/thumbnail/i",$img["title"])){
					$feature_image = $img["location"];
				}
			}
		}
	}

	//overwrite client name is post meta exists
	$testmeta = get_post_meta($item->ID,'client-name',true);
	if(preg_match("/\w/",$testmeta)){
		$clientName = $testmeta;
	}

	//overwrite project name is post meta exists
	unset($testmeta);
	$testmeta = get_post_meta($item->ID,'project-name',true);
	if(preg_match("/\w/",$testmeta)){
		$projectName = $testmeta;
	}
	else{
		$projectName = $item->post_title;
	}
	
	echo '<div class="featureblock bar" style="margin-right:' . (($columncount==3)?0:42) . 'px;">';
    echo '<a href="' . $feature_link . '" class="featureoverlay">';
	echo '<div class="details">';
	echo '<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">';

	echo '<span class="orange">' . $clientName . '</span> <span style="color:#bce0cd !important;"> | </span> <span style="color:#ffffff !important;">' . $projectName. '</span></p>';	

	echo '</div></a><div class="featurebackdrop"><img src="' . $feature_image . '" style="padding-top:0px !important;"/></div><!-- End #featurebackdrop --></div> <!-- End #featureblock -->';

	$columncount++;
	
}

?>

</div> <!--Featured Items -->
<br style="clear:left;"/>
</div> <!-- End serviceContent -->
         
		</div><!-- .entry-content -->
		<footer class="entry-meta">
			
		</footer><!-- .entry-meta -->
	</article><!-- #post -->

<?php

//old code 
/*
<! ---- Feature 1 ------>
		<div class="featureblock bar" style="margin-right:42px;">
        	<a href="<?php the_field('feature_link_1'); ?>" class="featureoverlay">
		<div class="details">
		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         	<span class="orange"><?php the_field('network_1'); ?></span> <span style="color:#bce0cd !important;"> | </span> <span style="color:#ffffff !important;"><?php the_field('show_or_movie_title_1'); ?></span></p>
		</div> <!-- End #details -->
                </a>
		<div class="featurebackdrop"><img src="<?php the_field('feature_image_1'); ?>" style="padding-top:0px !important;"/></div><!-- End #featurebackdrop -->
		</div> <!-- End #featureblock -->

<! ---- Feature 2 ------>

<div class="featureblock bar">
        	<a href="<?php the_field('feature_link_2'); ?>" class="featureoverlay">
		<div class="details">
		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         	<span class="orange"><?php the_field('network_2'); ?></span> <span style="color:#bce0cd !important;">|</span> <span style="color:#ffffff !important;"><?php the_field('show_or_movie_title_2'); ?></span></p>
		</div>
                </a>
		<div class="featurebackdrop"><img src="<?php the_field('feature_image_2'); ?>" style="padding-top:0px !important;"/></div>
		</div>

<! ---- Feature 3 ------>

		<div class="featureblock bar" style="float:right;">
        	<a href="<?php the_field('feature_link_3'); ?>" class="featureoverlay">
		<div class="details">
		<p style="padding-top:15px; padding-left:15px; padding-right:15px; padding-bottom:0px;-webkit-margin-before: 0px; margin-top: 0px;">
         	<span class="orange"><?php the_field('network_3'); ?></span> <span style="color:#bce0cd !important;">|</span> <span style="color:#ffffff !important;"><?php the_field('show_or_movie_title_3'); ?></span></p>
		</div>
                </a>
		<div class="featurebackdrop"><img src="<?php the_field('feature_image_3'); ?>" style="padding-top:0px !important;"/></div>
		</div>

*/

?>