<div id="social-buttons">
	<span class="mm_share_text"></span>
	<a href="https://www.facebook.com/hothouseinc" class="mm_facebook" target="new"><span>Like Us on Facebook</span></a>
	<a href="http://www.linkedin.com/company/hothouse-inc."  class="mm_linkedin"  target="new"><span>Follow Us on LinkedIn</span></a>
	<a href="http://www.twitter.com/HothouseInc" class="mm_twitter" target="new"><span>Follow us on Twitter</span></a>
</div>