<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
</div> <!-- end container -->
<div id="footer">
<?php

$address = new WP_Query(array('category_name'=>'footer'));
echo "\n<div id='footer-address'>\n\n";
echo $address->posts[0]->post_content;
echo "</div>\n\n";
echo "\n<div id='footer-links'>\n\n";
$categories=get_categories(array('parent'=>0,'hide_empty'=>0,'orderby'=>'id','exclude'=>'49,43,12'));
foreach($categories as $category) { 
	if($category->slug!="footer"){	
		if($category->slug=="home"){
			$category_link = "/blog/";
		}
		else if($category->slug=="services"){
			$category_link = "/services/";
		}
		else{
			$category_link = get_category_link($category->cat_ID);
		}
		
		/*if( $category->slug =="client-login"){
		echo "<a href='http://www.hothouseinc.com/clientloginext/login.php'>Client Login</a> ";
		}else{*/
		echo "<a href='" . $category_link  . "'>" . $category->name . "</a>\n";
		//}
		
		
	}
}//for each
echo "</div>\n\n";
?>
</div><!-- end main content container -->

<?php
echo "\n<script type=\"text/javascript\">\n";
if(isset($_SESSION["category"])){
	echo "var mainbutn = 'mm_" . $_SESSION["category"] . "';\n";
	echo "showHighlights(mainbutn)\n\n";
	if(isset($_SESSION["subcategory"])){
		echo "var submenubutn = 'sub_" . $_SESSION["subcategory"] . "';\n";
		echo "showHighlights(submenubutn)\n\n";
	}
	if(isset($_SESSION["post"])){
		echo "var postbutn = 'pm_" . $_SESSION["post"] . "';\n";
		echo "showHighlights(postbutn)\n\n";
	}
}
else{
	echo "showHighlights('mm_home');\n\n";
}
echo "</script>\n";

?>
</body>
</html>
