<?php
session_start();

$cache = "?t=" . time();

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="width=1024, user-scalable=yes" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	// Add the blog name.
	echo get_bloginfo( 'description', 'display' );
	?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?><?=$cache?>" />
<?php
	
$cat2 = get_the_category();


?>
<script type="text/javascript" src="/interface/javascript/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="/interface/javascript/browser.js<?=$cache?>"></script>
<script type="text/javascript" src="/interface/javascript/ticker.js"></script>
<script type="text/javascript" src="/interface/javascript/hothouse.js<?=$cache?>"></script>
<script type="text/javascript" src="/interface/javascript/swfobject2.1.js<?=$cache?>"></script>
<script type="text/javascript" src="/interface/javascript/mosaic.1.0.1.js<?=$cache?>"></script>
<script type="text/javascript" src="/interface/javascript/theme.js<?=$cache?>"></script>
<?php
echo "<script type=\"text/javascript\">\n\n";
echo "var ajaxURL = '" . admin_url() . "admin-ajax.php';\n";
echo "</script>\n";
?>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16001949-9']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
</head>
<body>
<noscript><div id="noscript">Please enable Javascript to take full advantage of this site.</div></noscript>
<div id="test"></div>
<?php
/*
echo "<pre>";
print_r($cat2);
echo "</pre>";
*/
?>
<div id="header">
<div id="logo"></div>
<?php
require_once("content-socialnavigation.php");
?>
<div id="navigation">
<?php 
//DON'T USE THIS!!!!!!!
//wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); 

$categories=get_categories(array('parent'=>0,'hide_empty'=>0,'orderby'=>'id', 'exclude'=>'49,43,12'));
foreach($categories as $category){
if($category->slug!="footer"){
	if($category->slug=="home"){
		$category_link = "/";
	}
	else if(preg_match("/services/i",$category->slug)){
		$category_link = "/services/";
	}
	else{
		$category_link = get_category_link($category->cat_ID);
	}
	echo "<a href='" . $category_link  . "' id='mm_" .  $category->slug . "' ";
	if(is_home() && $category->slug=='home'){
		echo "class='selected'";
	}
	elseif(!is_home() && $category->slug==$cat2[0]->slug){
		echo "class='selected'";
	}
	echo ">" . $category->name . "</a>\n";
}
}// end foreach
?>
<div class="breaker"></div>
</div><!-- #navigation -->
<?php
// if no parent then get sub categories
if($cat2[0]->parent==0 && $cat2[0]->slug!='home'){
	if($cat2[0]->cat_ID){
		$getsubcategories = get_categories(array(parent=>$cat2[0]->cat_ID));
	}
}
//has parent get sub categories for parent
elseif($cat2[0]->parent>0){
	$getsubcategories = get_categories(array('parent'=>$cat2[0]->parent,'orderby'=>'id'));
}
?>
</div> <!-- end header -->
<div id="container"><!-- begin main content container -->
<?php
//used to see if section is a work category
$parentCat = get_category($cat2[0]->parent);
if(preg_match("/about/i",$parentCat->name)){
	
	if(sizeof($getsubcategories)>0 && !is_home() && !preg_match("/work/i",$cat2[0]->slug)){
	
	echo "\n\n<div id='about_subnavigation'>\n";
	
	foreach($getsubcategories as $subcat) 
	{
		$subcat_link = get_category_link($subcat->cat_ID);
		echo "<a href='" . $subcat_link  . "' id='sub_" . $subcat->slug . "'>" . $subcat->slug . "</a>\n";
	}// end for each
	
	echo "<div class='breaker'></div>\n\n";
	echo "</div>\n\n"; 
	}//for each
}
?>