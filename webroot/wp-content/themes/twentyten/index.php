<?php 
get_header(); 

//used for the showcase items
$query = new WP_Query( 'category_name=showcase' );
?>
<script type="text/javascript">
var totalShowCaseItems = <?=count($query->posts)?>;
//images to check if loaded starts function
var imagesToPreload = Array();
<?php
$i = 0;
foreach($query->posts as $catPost){
		echo "\n\t\t" . "imagesToPreload[$i] = '#preload" . $i . "';";
		$i++;
}
?>
j(document).ready(
	function()
	{
		loadChecker = setInterval(startWebsite,100);
		
		j("#showcase1").css({"visibility":"visible","left": "0px"});
		
		if(!browsersniffer){
			
			/*
			j(".showcaseArea").bind('mouseover', function(){
				//j(this).find(".showCoverHolder").fadeTo(200,1);
				j("#showNext").show();
				j("#showPrev").show();
	
			});
			*/
			
			//j(".showcaseCoverText").bind('mouseover',function(){
			j(".showcaseArea").bind('mouseover',function(){	
				j(".showcaseCoverText").css("background-position","center center");
				j(".showcaseCover").fadeTo(200,.8);
			});
			
			//j(".showcaseCoverText").bind('mouseout',function(){
			j(".showcaseArea").bind('mouseout',function(){
				j(".showcaseCoverText").css("background-position","center -100px");
				j(".showcaseCover").hide();
			});
			
			/*
			j("#showNext,#showPrev").bind('mouseover',function(){
				j(this).show();
				//j(".showcaseArea").find(".showCoverHolder").show();
			});
			
			
			j("#showNext,#showPrev").bind('mouseout',function(){
				j(this).hide();
			});
			*/
		
			j(".showcaseArea").bind('mouseout', function(){
				//j(this).find(".showCoverHolder").hide();
				j("#showNext").hide();
				j("#showPrev").hide();
			});
			
			
			/*
			j("#showNext").bind('click', function(){
				homeNextPrev(false);	
				return false;
			});
			
			j("#showPrev").bind('click', function(){
				homeNextPrev(true);	
				return false;
			});
			*/
		}
	
	}
);
</script>
		
<?php 
/*
echo "<a href='' id='showcaseOverPrev'><span>Previous</span></a>\n";
echo "<a href='' id='showcaseOverNext'><span>Next</span></a>\n\n";
*/
echo "<div id='showcaseBack'>\n";
echo "<a href='' id='showPrev'></a><a href='' id='showNext'></a>\n";
echo "<div id='homeLoader'></div>\n\n";
$i=1;
foreach($query->posts as $catPost){
	$attachments = attachments_get_attachments($catPost->ID);
	$total_attachments = count($attachments);
	if($total_attachments>0){
		$backgroundImage = "background-image:url(" . $attachments[0]["location"]  . ");";
	}
	$customInfo = get_post_custom($catPost->ID);
	echo "<a href='" . $customInfo["projectpage"][0]  . "' class='showcaseArea' id='showcase" . $i .  "' style='" . (($backgroundImage)?$backgroundImage:"") ." text-decoration:none;'><span class='showcaseCoverText'></span><span class='showcaseCover'></span></a>\n";
	$i++;
}// end foreach
echo "</div>\n\n";
echo "<div id='showcasenav'></div>\n\n";
$homeContent = new WP_Query(array('category__in'=>1));
echo "<div id='homeinfo'>\n";
/*
if($homeContent->post->post_title){
	echo "<h1>" . $homeContent->post->post_title . "</h1>\n";
}
*/
echo $homeContent->post->post_content;
echo "\n</div>\n";
echo "<div id='homeNews'>\n";
$newsTitle = get_category_by_slug('news'); 
echo "<div id='newsTitle'>". $newsTitle->description . "</div>\n";
$newsQuery = get_posts(array('numberposts'=>3,'category_name'=>'news','orderby'=>'date','order'=>'DESC','meta_key' =>'frontburner','meta_value' =>'true'));
//gets id of posts selected for front burner
//if less than 3 gets a non front burner record
foreach($newsQuery as $newsItem){
	$ids[] = $newsItem->ID;
}
//if there are less than 3 front burner items
if(sizeof($ids)<3){
	
	//creates an exclude id string
	for($i=0;$i<sizeof($ids);$i++){
		$excludeids .= $ids[$i];
		if($ids[$i+1]){
			$excludeids .= ",";
		}
	}
	
	//gets difference between found front burner records and 
	
	$newsQueryAdditional = get_posts(array('numberposts'=>(3 - sizeof($ids)),'category_name'=>'news','orderby'=>'date','exclude'=>$excludeids,'order'=>'DESC'));
	
	//gets id of posts selected for front burner
	//if less than 3 gets a non front burner record
	
	$ids="";
	
	foreach($newsQueryAdditional as $newsItem){
		$ids[] = $newsItem->ID;
	}
	
	//creates an include id string
	for($i=0;$i<sizeof($ids);$i++){
		$includeids .= $ids[$i];
		if($ids[$i+1]){
			$includeids .= ",";
		}
	}
	
	//concactenantes excluded ids to included ids
	$includeids .= "," . $excludeids;
	
	$newsQuery = get_posts(array('numberposts'=>3,'category_name'=>'news','orderby'=>'date','order'=>'DESC','include'=>$includeids));
	
}
foreach($newsQuery as $newsItem){
echo "<div class='newsColumn'>\n";
$attachments = attachments_get_attachments($newsItem->ID);
$total_attachments = count($attachments);
if($total_attachments>0){
	for($i=0;$i<$total_attachments;$i++){
		if(preg_match("/home/i",$attachments[$i]["title"])){
			echo "<a href='/category/news/#fb" . $newsItem->ID  . "'><img src='" . $attachments[$i]["location"] . "' width='257' height='168' alt=\"" . $attachments[$i]["title"] . "\"/></a>\n";
		}
	}//end for
}
//$newsDate = date('F j, Y', strtotime($newsItem->post_date));
//$newsDate = date('F Y', strtotime($newsItem->post_date));
echo "<span>" . $newsItem->post_title . "</span>\n\n";
echo "<b>" . $newsDate . "</b>";
echo $newsItem->post_excerpt;
echo "<a href='/category/news/#fb" . $newsItem->ID  . "'>More...</a>\n";
echo "</div>\n";
}//foreach
echo "<div class='breaker'></div>";
echo "</div>";
//$temp=get_categories(array('parent'=>0,'hide_empty'=>0,'orderby'=>'id'));
unset($_SESSION["category"]);
unset($_SESSION["subcategory"]);
unset($_SESSION["post"]);
echo "\n\n\n<div id='preloadArea'>\n";
$i=0;
foreach($query->posts as $catPost){
	$attachments = attachments_get_attachments($catPost->ID);
	$total_attachments = count($attachments);
	if($total_attachments>0){
		echo "<img src='" . $attachments[0]["location"] . "' width='1' height='1' id='preload" . $i . "' alt=''/>\n";
	}
	$i++;
}
echo "</div>\n\n";
?>
<?php get_footer(); ?>
