<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php if ( ! is_page_template( 'page-templates/front-page.php' ) ) : ?>
			<?php the_post_thumbnail(); ?>
			<?php endif; ?>
			<h1 id="pageTitle"><?php the_title(); ?></h1>
		</header>

		<div class="entry-content">
		<?php the_content(); ?>
        <?php $services_loop = new WP_Query( array('post_type' => 'services', 'posts_per_page' => '20', 'orderby' => 'menu_order' ) ); ?>
 
        <?php while ( $services_loop->have_posts() ) : $services_loop->the_post(); ?>
                       
         <div id="service" class="left">
         <div id="box">
         <div id="thumbnail"><img src="<?php the_field('service_thumbnail'); ?>"/></div>
         <div id="rollover" class="object move-up">
         <p style="padding: 0px 15px;">
         <span class="orange"><?php the_field('service_title'); ?> </span><br /><br />
		 <span style="color:#fff !important;"><?php the_field('service_excerpt'); ?></span><br /><br />
         <a href="<?php the_field('service_page_link'); ?>"><span style="color:#fff !important;">Learn More</span></a></p>
         </div> <!-- End #Box -->
         </div> <!-- End #Rollover -->
         </div><!-- End #Service-->
		 
		 <?php endwhile; ?>
         
         <br style="clear:left;"/>
			
            
			
		</div><!-- .entry-content -->
		<footer class="entry-meta">
			
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
