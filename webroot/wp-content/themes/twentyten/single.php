<?php 
ob_start();

$ru = preg_replace("/\//","",$_SERVER["REQUEST_URI"]);

if(!preg_match("/category/i",$ru) && preg_match("/\w/",$ru)){

	global $post;
	$post_cat = get_the_category($post->ID);

	if(sizeof($post_cat)==1 && $post_cat[0]->slug=="news"){
		header("Location: /category/news/#fb" . $post->ID);
		exit();
	}
}

get_header(); 

$catID =  get_the_category();

$parentCat = get_category($catID[0]->category_parent);

//$subnavigation = new WP_Query('cat=' . $catID[0]->cat_ID);


$subnavigation = get_posts(array('numberposts'=>0,'category'=>$catID[0]->cat_ID,'orderby'=>'id','order'=>'ASC'));
	
if($catID[0]->category_description){
	echo "<div id='pageTitle'>" . $catID[0]->category_description ."</div>\n";
}


if(preg_match("/expertise/i",$catID[0]->slug)){

	echo "<div id='content'>\n\n";	
	echo "<div id='expertContent'>\n";
	
	echo "<div id='expertNav'>\n";
	foreach($subnavigation as $subpost){
			echo "<a href='/blog/" . $subpost->post_name ."'><span id='pm_".  $subpost->ID . "'>" . $subpost->post_title . "</span></a>\n";
			// old echo "<a href='/blog/" . $subpost->post_name ."' id='pm_".  $subpost->ID . "'>" . $subpost->post_title . "</a>\n";
	}
	echo "</div>\n";
	
}
else{
	
	
	
	if(sizeof($subnavigation)>1){	
	
	echo "<div id='sectionSubNav' class='typefilter'>\n";
	echo "<span class='filterTitle'>Select an Item:</span>\n";
	echo "<a class='filterSelected' id='filselec-type'>" . $catID[0]->name . "</a>\n";

	echo "<div id='filtdrop-type' class='filterdrop'>\n";
	foreach($subnavigation as $subpost){
		echo "<a href='' id='f-".  $subpost->ID . "' onclick=\"javascript: workTriage('". $subpost->ID ."'); return false;\">" . $subpost->post_title . "</a>\n";
	}
		
	echo "</div>\n\n";
		
	echo "</div><!-- end sectionSubNav  -->\n\n";		
	echo "<div class='breaker'></div>\n\n";				

	}
	
	/*
	if(sizeof($subnavigation)>1){
	echo "<div id='sectionSubNav'>\n";

		foreach($subnavigation as $subpost){
			echo "<a href='/blog/" . $subpost->post_name ."' id='pm_".  $subpost->ID . "'>" . $subpost->post_title . "</a>\n";
		}
			
	echo "</div><!-- end sectionSubNav  -->\n\n";
	echo "<div class='breaker'></div>\n\n";
	}
	*/

	echo "<div id='content'>\n\n";	

	echo "<div id='sectionContent'>\n";
	
	
}
?>


<?php

	if(preg_match("/expertise/i",$catID[0]->slug)){
		echo "<script type='text/javascript' src='/interface/javascript/expertise.js'></script>";
		echo "<div class='expertColumn'>\n\n";
	}
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


	<?php 

	if($catID[0]->parent>0){
		$temp = get_category($catID[0]->parent);
		$_SESSION["category"] = $temp->slug;
		$_SESSION["subcategory"] = $catID[0]->slug;
	}
	else{
		$_SESSION["category"] = $catID[0]->slug;
		unset($_SESSION["subcategory"]);
	}
	
		
	$_SESSION["post"] = $post->ID;


	if($post->post_title && !preg_match("/\<h1/i",$post->post_content)){
		if($catID[0]->slug == 'news'){
			
				if(get_post_meta($post->ID,'date',true)){
					$newsDate =  get_post_meta($post->ID,'date',true);
				}
				else{
					$newsDate = date('F j, Y', strtotime($post->post_date));
				}

				echo "<div id='postTitleDate'>"  . $newsDate . "</div>\n\n";
				
		}
		
		echo "<div id='postTitle'>" . $post->post_title . "</div>\n";
	}
	
	

	echo $post->post_content;
	/*
	$tempPhoto = get_post_meta($post->ID, 'photo');
	if($tempPhoto){
		echo "<img src='" . $tempPhoto[0] . "' />";
	}
	*/

?>

<?php endwhile; // end of the loop. ?>

<?php	

if(preg_match("/expertise/i",$catID[0]->slug)){
	echo "</div><!-- end expertColumn -->\n";
}

echo "</div><!-- end sectionContent and expertContent -->\n";
echo "<div class='breaker'></div>\n";
echo "</div><!-- end content  -->\n";


/*
$attachments = attachments_get_attachments();
$total_attachments = count($attachments);

if( $total_attachments > 0 ){
	for ($i=0; $i < $total_attachments; $i++){
	echo "<img src='" . <?=$attachments[$i]["location"]?> . "'/>"';
	}
}
*/
?>




<?php get_footer(); ?>




